using System.ComponentModel.DataAnnotations;

namespace LoginRegisterProject.API.Models.Entity
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string UserName  { get; set; }
        public string Number { get; set; }
        public string Mail { get; set; }
        public string Password { get; set; }
    }
}