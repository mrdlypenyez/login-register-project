
using System.Data.Entity;
using System.IO;
using LoginRegisterProject.API.Models.Entity;
using MySql.Data.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using DbContext = Microsoft.EntityFrameworkCore.DbContext;


namespace LoginRegisterProject.API.Models.Context
{ 
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    
    public class LoginSystemDbContext:DbContext
    {
        public Microsoft.EntityFrameworkCore.DbSet<User> Users { get; set; }

        public LoginSystemDbContext() : base(){}
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();
                var connectionString = configuration.GetConnectionString("MySQLZeynep");
                optionsBuilder.UseMySQL(connectionString);

            }
        }
       
    }
}